import { PermissionsAndroid } from 'react-native';

const requestVoicePermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, {
      title: 'Audio record permission',
      message: 'Send voice command to the server.',
      buttonNeutral: 'Ask Me Later',
      buttonNegative: 'Cancel',
      buttonPositive: 'OK',
    });
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use the audio recording');
    } else {
      console.log('Audio reording permission denied');
    }
  } catch (err) {
    console.warn(err);
  }
};

export default requestVoicePermission;
